package com.gl.repository;

import com.gl.model.Person;  
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Integer>{  
}
